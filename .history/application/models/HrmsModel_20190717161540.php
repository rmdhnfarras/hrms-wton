<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

class HrmsModel extends CI_model {

    //Retrieve all data from TB_PAT table
    public function getAllData() {
        return $this->db->query("SELECT * FROM TB_PAT")->result();
    }

    //Insert data to TB_PAT
    public function create($kdpat, $ket, $alamat, $kota, $singkatan, $createdby) {
        $query = $this->db->query("INSERT INTO TB_PAT (KD_PAT, 
                                                        KET, 
                                                        ALAMAT, 
                                                        KOTA, 
                                                        SINGKATAN, 
                                                        CREATED_BY, 
                                                        CREATED_DATE, 
                                                        LAST_UPDATE_BY, 
                                                        LAST_UPDATE_DATE, 
                                                        LAT_GPS, 
                                                        LNG_GPS) 
                                    VALUES ('".$kdpat."', 
                                            '".$ket."', 
                                            '".$alamat."', 
                                            '".$kota."', 
                                            '".$singkatan."', 
                                            '".$createdby."', 
                                            SYSDATE, 
                                            NULL, 
                                            NULL, 
                                            NULL, 
                                            NULL)")->result();

        if($query)
            return true;
        else
            return false;
    }

    //Edit or Update data to TB_PAT
    public function update($kdpat, $ket, $alamat, $kota, $singkatan, $lastupdateby) {
        $query = $this->db->query("UPDATE
                                        TB_PAT
                                    SET
                                        KET = '".$ket."',
                                        ALAMAT = '".$alamat."',
                                        KOTA = '".$kota."',
                                        SINGKATAN = '".$singkatan."',
                                        LAST_UPDATE_BY = '".$lastupdateby."',
                                        LAST_UPDATE_DATE = SYSDATE
                                    WHERE
                                        KD_PAT = '".$kdpat."'")->result();

        if($query)
            return true;
        else
            return false;   
    }

    //Delete data from TB_PAT
    public function delete($kdpat) {
        $this->db->where('KD_PAT', $kdpat);
        $query = $this->db->delete('TB_PAT');
        
        if($query)
            return true;
        else
            return false;
    }

    //Function for Filter in Kendo grid
    public function kendoFilter($filters) {
        
        if(isset($filters['operator'])) {
            if($filters['operator'] == 'eq') {
                $this->db->where($filters['field'], $filters['value']);
            }
            else if($filters['operator'] == 'neq') {
                $field = $filters['field'] . '!=';
                $this->db->where($field, $filters['value']);
            }
            else if($filters['operator'] == 'doesnotcontain') {
                $this->db->not_like($filters['field'], $filters['value']);
            }
            else if($filters['operator'] == 'contains') {
                $this->db->like($filters['field'], $filters['value'], 'both');
            }
            else if($filters['operator'] == 'startswith') {
                $this->db->like($filters['field'], $filters['value'], 'after');
            }
            else if($filters['operator'] == 'endswith') {
                $this->db->like($filters['field'], $filters['value'], 'before');
            }
            else if($filters['operator'] == 'isnull') {
                $field = $filters['field'] . ' IS';
                $this->db->where($filters['field'], $filters['value']);
            }
            else if($filters['operator'] == 'isnotnull') {
                $field = $filters['field'] . ' IS NOT';
                $this->db->where($filters['field'], $filters['value']);
            }
            else if($filters['operator'] == 'isempty') {
                $field = $filters['field'] . '==';
                $this->db->where($filters['field'], $filters['value']);
            }
            else if($filters['operator'] == 'isnotempty') {
                $field = $filters['field'] . '!=';
                $this->db->where($filters['field'], $filters['value']);
            }
        } else {
            getAllData();
        }
        $query = $this->db->get('TB_PAT')->result();

        if($query)
            return $query;
        else
            return false; 
    }

    //Function for Sorting data from database
    public function kendoSort($sort_field, $sort_dir) {
     
        $query = $this->db->query("SELECT *  
                                    FROM TB_PAT
                                    ORDER BY ".$sort_field." ".$sort_dir)->result();  

        if($query)
            return $query;
        else
            return false; 
    }
}
