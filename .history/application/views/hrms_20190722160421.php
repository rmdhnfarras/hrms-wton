<!DOCTYPE html>
<html>
<head>
    <title>HRMS WTON</title>
    <link rel="stylesheet" href="assets/telerik-php/styles/kendo.material-v2.min.css" />
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.material.min.css" /> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.material.mobile.min.css" /> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.common.min.css"/> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.default.min.css"/> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.default.mobile.min.css"/> -->
    
    <script src="assets/telerik-php/js/jquery.min.js"></script>
    <script src="assets/telerik-php/js/kendo.all.min.js"></script>
</head>
<body>

<div id="example">
    <div id="grid"></div>
    <script>
        $(document).ready(function () {
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {

                        read: {
                            url: "<?php echo base_url(); ?>HrmsController/getAllList",
                            dataType: "json",
                            type: "post",
                        },
                      
                        create: {
                            url: "<?php echo base_url(); ?>HrmsController/create",
                            dataType: "json",
                            type: "post",
                            complete: function(e) {
                                $("#grid").data("kendoGrid").dataSource.read(); 
                                $("#grid").data("kendoGrid").refresh(); 
                            }
                        },

                        update: {
                            url: "<?php echo base_url(); ?>HrmsController/update",
                            dataType: "json",
                            type: "post",
                            complete: function(e) {
                                $("#grid").data("kendoGrid").dataSource.read(); 
                            }
                        },

                        destroy: {
                            url: "<?php echo base_url(); ?>HrmsController/destroy",
                            dataType: "json",
                            type: "post"
                        }

                    },
                    schema: {
                        data: "user_data",
                        model: {
                            id: "KD_PAT",
                            fields: {
                                KD_PAT: { validation: { required: true } },
                                KET: { validation: { required: true } },
                                ALAMAT: { validation: { required: true } },
                                KOTA: { validation: { required: true } },
                                SINGKATAN: { validation: { required: true } },
                            }
                        },
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                },
                batch: true,
                height: 700,
                groupable: true,
                sortable: true,
                reorderable: true,
                filterable: {extra: false},
                selectable: "Single, Row",
                
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                editable: "inline",
                toolbar: [{name: "create", text: "Add New Data"}],

                //Grid Hierarcy
                detailInit: detailInit,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },

                columns: [{ 
                        field: "KD_PAT",
                        title: "Kode PAT",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "KET",
                        title: "Ket",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "ALAMAT",
                        title: "Alamat",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "KOTA",
                        title: "Kota",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "SINGKATAN",
                        title: "Singkatan",
                        "filterable": true,
                        "encoded": true
                    }, {
                        title: "Option",
                        width: 300,
                        command: [
                            { name: "edit", text: { edit: "Edit", update: "Update", cancel: "Cancel" } },
                            { name: "destroy", text: "Delete" }
                        ]
                    }]
            });
            //Grid Hierarcy
            function detailInit(e) {
                $("<div/>").appendTo(e.detailCell).kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                url: "<?php echo base_url(); ?>HrmsController/getAllList",
                                dataType: "json",
                                type: "post",
                            },
                            create: {
                                url: "<?php echo base_url(); ?>HrmsController/createDetail",
                                dataType: "json",
                                type: "post",
                                data: {value: e.data.KD_PAT},
                                complete: function(e) {
                                    $("#grid").data("kendoGrid").dataSource.read(); 
                                    $('<div class="k-widget k-window" data-role="draggable" style="visibility: visible; display: flex; padding-top: 70px; min-width: 90px; min-height: 50px; top: 221.5px; left: 200.5px; z-index: 10004; opacity: 1; transform: scale(1);"><div class="k-window-titlebar k-header" style="margin-top: -70px;"><span class="k-window-title">Edit</span><div class="k-window-actions"><a role="button" href="#" class="k-button k-bare k-button-icon k-window-action" aria-label="Close"><span class="k-icon k-i-close"></span></a></div></div><div data-uid="7f626453-b017-427c-ba7b-9f6fc83a155c" class="k-popup-edit-form k-window-content k-content" data-role="window" tabindex="0" style=""><div class="k-edit-form-container"><div class="k-edit-label"><label for="KD_PAT">Kode PAT</label></div><div class="k-edit-field">0A</div><div class="k-edit-label"><label for="KD_GAS">Kode GAS</label></div><div data-container-for="KD_GAS" class="k-edit-field"><input type="text" class="k-input k-textbox" name="KD_GAS" required="required" data-bind="value:KD_GAS"></div><div class="k-edit-label"><label for="KET">Keterangan</label></div><div class="k-edit-field">Biro Engineering</div><div class="k-edit-buttons k-state-default"><a role="button" class="k-button k-button-icontext k-primary k-grid-update" href="#"><span class="k-icon k-i-check"></span>Update</a><a role="button" class="k-button k-button-icontext k-grid-cancel" href="#"><span class="k-icon k-i-cancel"></span>Cancel</a></div></div></div></div>').css('visibility', 'hidden');
                                    //hide toggle
                                }
                            },

                            update: {
                                url: "<?php echo base_url(); ?>HrmsController/updateDetail",
                                dataType: "json",
                                type: "post",
                                data: {oldkdgas: e.data.KD_GAS},
                                complete: function(e) {
                                    $("#grid").data("kendoGrid").dataSource.read(); 
                                }
                            },

                            destroy: {
                                url: "<?php echo base_url(); ?>HrmsController/destroyDetail",
                                dataType: "json",
                                type: "post"
                            }
                        },
                        schema: {
                            data: "user_data",
                            model: {
                                id: "KD_PAT",
                                fields: {
                                    KD_PAT: { editable: false },
                                    KD_GAS: { validation: { required: true } },
                                    KET: { editable: false },
                                }
                            },
                        },
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        pageSize: 10,
                        filter: { field: "KD_PAT", operator: "eq", value: e.data.KD_PAT }
                    },
                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    editable: "popup",
                    toolbar: [{name: "create", text: "Add New Data"}],
                    columns: [
                        { 
                            field: "KD_PAT", 
                            title: "Kode PAT" 
                        }, { 
                            field: "KD_GAS", 
                            title: "Kode GAS",
                            // editor: productDropDownEditor,
                            // template: "#=KD_GAS#" 
                        }, { 
                            field: "KET", 
                            title: "Keterangan"
                        }, {
                            title: "Option",
                            width: 300,
                            command: [
                                { name: "edit", text: { edit: "Edit", update: "Update", cancel: "Cancel" } },
                                { name: "destroy", text: "Delete" }
                            ]
                        }
                    ]
                });
            }
            /* -- custom drop down editors */
            // function productDropDownEditor(container, options) {
            //     $('<input data-text-field="KD_GAS" data-value-field="KET" data-bind="value:' + options.field + '"/>')
            //         .appendTo(container)
            //         .kendoDropDownList({
            //             autoBind: false,
            //             dataSource: {
            //                 transport: {
            //                     read: {
            //                         url: "<?php echo base_url(); ?>HrmsController/getTbGasList",
            //                         dataType: "json",
            //                     }
            //                 }
            //             },
            //             dataTextField: "KD_GAS",
            //             dataValueField: "KET"
            //         });
            // }
            // $("#grid").data("kendoGrid").dataSource.fetch(function() {
            //     console.log($("#grid").data("kendoGrid").dataSource.view().length); // displays how many rows in the view
            // });       
            $("#grid").delegate("tbody>tr", "click", function () {
                if (!$(this).hasClass('k-grid-edit')) {
                    console.log('test'); 
                    
                    
                    console.log($('<input type="text" class="k-input k-textbox" name="KD_GAS" required="required" data-bind="value:KD_GAS">'));
                }
            });
        });
    </script>
</div>

<style type="text/css">

</style>


</body>
</html>