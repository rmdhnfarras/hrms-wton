<!DOCTYPE html>
<html>
<head>
    <title>HRMS WTON</title>
    <link rel="stylesheet" href="assets/telerik-php/styles/kendo.material-v2.min.css" />
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.material.min.css" /> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.material.mobile.min.css" /> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.common.min.css"/> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.default.min.css"/> -->
    <!-- <link rel="stylesheet" href="assets/telerik-php/styles/kendo.default.mobile.min.css"/> -->
    
    <script src="assets/telerik-php/js/jquery.min.js"></script>
    <script src="assets/telerik-php/js/kendo.all.min.js"></script>
</head>
<body>

<div id="example">
    <div id="grid"></div>
    <script>
        $(document).ready(function () {
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {

                        read: {
                            url: "<?php echo base_url(); ?>HrmsController/getAllList",
                            dataType: "json",
                            type: "post",
                        },
                      
                        create: {
                            url: "<?php echo base_url(); ?>HrmsController/create",
                            dataType: "json",
                            type: "post",
                            complete: function(e) {
                                $("#grid").data("kendoGrid").dataSource.read(); 
                            }
                        },

                        update: {
                            url: "<?php echo base_url(); ?>HrmsController/update",
                            dataType: "json",
                            type: "post",
                            complete: function(e) {
                                $("#grid").data("kendoGrid").dataSource.read(); 
                            }
                        },

                        destroy: {
                            url: "<?php echo base_url(); ?>HrmsController/destroy",
                            dataType: "json",
                            type: "post"
                        }

                    },
                    schema: {
                        data: "user_data",
                        model: {
                            id: "KD_PAT",
                            fields: {
                                KD_PAT: { validation: { required: true } },
                                KET: { validation: { required: true } },
                                ALAMAT: { validation: { required: true } },
                                KOTA: { validation: { required: true } },
                                SINGKATAN: { validation: { required: true } },
                            }
                        },
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                },
                batch: true,
                height: 700,
                groupable: true,
                sortable: true,
                reorderable: true,
                filterable: {extra: false},
                selectable: "Single, Row",
                
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                editable: "inline",
                toolbar: [{name: "create", text: "Add New Data"}],

                //Grid Hierarcy
                detailInit: detailInit,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },

                columns: [{ 
                        field: "KD_PAT",
                        title: "Kode PAT",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "KET",
                        title: "Ket",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "ALAMAT",
                        title: "Alamat",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "KOTA",
                        title: "Kota",
                        "filterable": true,
                        "encoded": true
                    }, { 
                        field: "SINGKATAN",
                        title: "Singkatan",
                        "filterable": true,
                        "encoded": true
                    }, {
                        title: "Option",
                        width: 300,
                        command: [
                            { name: "edit", text: { edit: "Edit", update: "Update", cancel: "Cancel" } },
                            { name: "destroy", text: "Delete" }
                        ]
                    }]
            });
            //Grid Hierarcy
            function detailInit(e) {
                $("<div/>").appendTo(e.detailCell).kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                url: "<?php echo base_url(); ?>HrmsController/getAllDetailList",
                                dataType: "json",
                                type: "post",
                            },
                            create: {
                                url: "<?php echo base_url(); ?>HrmsController/createDetail",
                                dataType: "json",
                                type: "post",
                                complete: function(e) {
                                    $("#grid").data("kendoGrid").dataSource.read(); 
                                    //hide toggle
                                }
                            },

                            update: {
                                url: "<?php echo base_url(); ?>HrmsController/updateDetail",
                                dataType: "json",
                                data: 'xxx',
                                type: "post",
                                complete: function(e) {
                                    $("#grid").data("kendoGrid").dataSource.read(); 
                                }
                            },

                            destroy: {
                                url: "<?php echo base_url(); ?>HrmsController/destroyDetail",
                                dataType: "json",
                                type: "post"
                            }
                        },
                        schema: {
                            data: "detail_data",
                            model: {
                                id: "KD_PAT",
                                fields: {
                                    KD_PAT: { validation: { required: true } },
                                    KD_GAS: { validation: { required: true } },
                                    KET: { editable: false },
                                }
                            },
                        },
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        pageSize: 10,
                        filter: { field: "KD_PAT", operator: "eq", value: e.data.KD_PAT }
                    },
                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    editable: "popup",
                    toolbar: [{name: "create", text: "Add New Data"}],
                    columns: [
                        { 
                            field: "KD_PAT", 
                            title: "Kode PAT" 
                        }, { 
                            field: "KD_GAS", 
                            title: "Kode GAS" 
                        }, { 
                            field: "KET", 
                            title: "Keterangan"
                        }, {
                            title: "Option",
                            width: 300,
                            command: [
                                { name: "edit", text: { edit: "Edit", update: "Update", cancel: "Cancel" } },
                                { name: "destroy", text: "Delete" }
                            ]
                        }
                    ]
                });
            }
            // $("#grid").data("kendoGrid").dataSource.fetch(function() {
            //     console.log($("#grid").data("kendoGrid").dataSource.view().length); // displays how many rows in the view
            // });    
        });
    </script>
</div>

<style type="text/css">

</style>


</body>
</html>