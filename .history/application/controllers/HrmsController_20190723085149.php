<?php

class HrmsController extends CI_Controller {
    
    public function __construct() {
        parent ::__construct();
        
        //load model
        $this->load->model('HrmsModel');
    }

    public function index() {
        $data = array(
            'user_data' => $this->HrmsModel->getAllData(),
        );
        $new_data = json_encode($data);
        $this->load->view('hrms', $new_data);
    }
    public function getAllList() {
        $skip       = $this->input->post('skip');
        $page       = $this->input->post('page');
        $filter     = $this->input->post('filter');
        $sort       = $this->input->post('sort');
        $pageSize   = $this->input->post('pageSize');
        $tableType  = $this->input->post('0');

        // print_r($tableType);

        if($filter != '') { //filter kendo
            $filters = array();
            $filters['logic']       = $filter['logic'];
            $filters['field']       = $filter['filters'][0]['field'];
            $filters['operator']    = $filter['filters'][0]['operator'];
            $filters['value']       = $filter['filters'][0]['value'];
            $val                    = $filters['value'];

            if($val != '') {
                $data = $this->HrmsModel->getAllDetailData($val);
            } else {
                $data = $this->HrmsModel->kendoFilter($filters);
            }
        } elseif ($sort != '') { //sort kendo
            $sort_field     = $sort[0]['field'];
            $sort_dir       = $sort[0]['dir'];

            $data = $this->HrmsModel->kendoSort($sort_field, $sort_dir);
        } elseif ($pageSize != '') {
            $data = $this->HrmsModel->kendoPageSize($pageSize);
        } else { //get all data
            $data = $this->HrmsModel->getAllData();
        }

        $return['user_data'] = $data;

        echo json_encode($return);
    }

    public function create() {
        $createdby         = 'Farras';
        
        $kdpat             = $this->input->post('KD_PAT');
        $ket               = $this->input->post('KET');
        $alamat            = $this->input->post('ALAMAT');
        $kota              = $this->input->post('KOTA');
        $singkatan         = $this->input->post('SINGKATAN');
        
        $this->HrmsModel->create($kdpat, $ket, $alamat, $kota, $singkatan, $createdby);         
    }
    public function update() {
        $lastupdateby      = 'Farras';
        
        $kdpat             = $this->input->post('KD_PAT');
        $ket               = $this->input->post('KET');
        $alamat            = $this->input->post('ALAMAT');
        $kota              = $this->input->post('KOTA');
        $singkatan         = $this->input->post('SINGKATAN');

        $this->HrmsModel->update($kdpat, $ket, $alamat, $kota, $singkatan, $lastupdateby);
    }

    public function destroy() {
        $kdpat  = $this->input->post('KD_PAT');

        $this->HrmsModel->delete($kdpat);   
    }
    //Details Data
    public function getAllDetailList() {
        $filters            = array();
        $filter             = $this->input->post('filter');
        $filters['value']   = $filter['filters'][0]['value'];
        

        

        $return['detail_data'] = $details;

        echo json_encode($return);
    }

    public function getTbGasList() {
        $data = $this->HrmsModel->getTbGasList();
        // $return['list_data'] = $data;

        echo json_encode($data);
    }
    public function createDetail() {
        $createdby      = 'Farras';
        
        // $kdpat          = $this->input->post('KD_PAT');
        $kdgas          = $this->input->post('KD_GAS');
        $value          = $this->input->post('value');
        
        $this->HrmsModel->createDetail($value, $kdgas, $createdby);         
    }
    public function updateDetail() {
        $lastupdateby   = 'Farras';
        
        $kdpat          = $this->input->post('KD_PAT');
        $kdgas          = $this->input->post('KD_GAS');
        $ket            = $this->input->post('KET');
        // $value          = $this->input->post('value');

        $this->HrmsModel->updateDetail($kdpat, $kdgas, $ket, $lastupdateby);
    }

    public function destroyDetail() {
        $kdpat  = $this->input->post('KD_PAT');
        $kdgas  = $this->input->post('KD_GAS');

        $this->HrmsModel->deleteDetail($kdpat, $kdgas);   
    }
}